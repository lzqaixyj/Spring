package com.lzq.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@PropertySource("classpath:jdbc.properties")
public class DemoDataSourceConfig {
    @Value("${jdbc.demo.url}")
    private String url;
    @Value("${jdbc.demo.driver}")
    private String driver;
    @Value("${jdbc.demo.user}")
    private String user;
    @Value("${jdbc.demo.password}")
    private String password;

    @Bean("demoDataSource")
    public DataSource getDatasource(){
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }
}
