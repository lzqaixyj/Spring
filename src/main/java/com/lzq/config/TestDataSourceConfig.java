package com.lzq.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

//<context:property-placeholder location="classpath:jdbc.properties"/>
@PropertySource("classpath:jdbc.properties")
public class TestDataSourceConfig {
    @Value("${jdbc.test.driver}")
    private String driver;
    @Value("${jdbc.test.url}")
    private String url;
    @Value("${jdbc.test.user}")
    private String user;
    @Value("${jdbc.test.password}")
    private String password;

    @Bean("testDataSource")
    public DataSource getDataSource(){
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }
}
