package com.lzq.config;

import com.alibaba.druid.pool.DruidPooledConnection;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

//<context:component-scan base-package="com.lzq"/>
@ComponentScan("com.lzq")  //配置组件扫描
//<import resource="jdbc.xml"/>

//@Import({TransactionDriverConfig.class})  //导入数据源配置  相当于xml中的import
public class SpringConfig {

}
