package com.lzq.dao.impl;

import com.lzq.dao.UserDao;
import com.lzq.model.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;
import java.util.Properties;

//<bean id="userDao" class="com.lzq.dao.impl.UserDaoImpl"></bean>
//@Component("userDao")
@Repository("userDao")
public class UserDaoImpl implements UserDao {

    private List<String> list;
    private Map<String, User> map;
    private Properties properties;



    private String name;
    private int age;

    public UserDaoImpl(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public void setMap(Map<String, User> map) {
        this.map = map;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    /**
     * 无参构造方法
     */
    public UserDaoImpl() {
        System.out.println("创建了userDaoImpl实例");
    }

    @PostConstruct  //bean初始化方法，相当于 xml里的 init-method
    public void init(){
        System.out.println("初始化。。。。");
    }
    @PreDestroy  //bean销毁方法，相当于 xml里的 destroy-method
    public void destroy(){
        System.out.println("销毁。。。。。");
    }
    public void save() {
//        System.out.println(list);
//        System.out.println(properties);
//        System.out.println(map);
//        System.out.println(name+"====="+age);
        System.out.println("保存中。 。。。。.");
    }
}
