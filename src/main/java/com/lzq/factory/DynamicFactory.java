package com.lzq.factory;

import com.lzq.dao.UserDao;
import com.lzq.dao.impl.UserDaoImpl;

/**
 * 工厂实例方法实例化bean
 */
public class DynamicFactory {
    public UserDao getUserDao(){
        return new UserDaoImpl();
    }
}
