package com.lzq.factory;

import com.lzq.dao.UserDao;
import com.lzq.dao.impl.UserDaoImpl;

/**
 * 工厂静态方法实例化bean
 *
 */
public class StaticFactory {
    public static UserDao getUserDao(){
        return new UserDaoImpl();
    }
}
