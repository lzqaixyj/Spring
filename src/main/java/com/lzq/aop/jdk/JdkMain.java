package com.lzq.aop.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 基于jdk动态代理技术
 */
public class JdkMain {
    public static void main(String[] args) {
        //目标对象
        final Target target = new Target();
        //增强对象
        final Invice invice = new Invice();
        //返回值就是动态代理生成的对象
        TargetInterface t = (TargetInterface) Proxy.newProxyInstance(
                target.getClass().getClassLoader(),  //目标对象类加载器
                target.getClass().getInterfaces(),   //目标对象相同的接口字节码数组
                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        invice.be();                //前置增强
                        Object o=method.invoke(target,args);//执行目标方法
                        invice.af();                //后置增强
                        return o;
                    }
                });
        //调用代理对象的方法
        t.save();
    }
}
