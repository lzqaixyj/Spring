package com.lzq.aop.cglib;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibMain {
    public static void main(String[] args) {
        //目标对象
        final Target target = new Target();
        //增强对象
        final Invice invice = new Invice();
        //返回值是动态代理对象，基于cglib
        //1、创建增强器
        Enhancer enhancer = new Enhancer();
        //2、设置父类（目标）
        enhancer.setSuperclass(target.getClass());
        //3、设置回调
        enhancer.setCallback(new MethodInterceptor() {
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                invice.be();
                Object o1 =  method.invoke(target,objects);
                invice.af();
                return o1;
            }
        });
        //4、创建代理对象
        Target t = (Target) enhancer.create();
        t.save();
        t.delete();
    }
}
