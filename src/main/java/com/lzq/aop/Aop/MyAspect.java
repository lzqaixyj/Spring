package com.lzq.aop.Aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component("aspect")
@Aspect   //标注aspect是一个切面类
public class MyAspect {
    @Before("execution(* com.lzq.aop.Aop.Target.*(..))")
    public void be(){
        System.out.println("前置增强.......");
    }
    @After("execution(* com.lzq.aop.Aop.Target.*(..))")
    public void after(){
        System.out.println("后置增强。。。。。");
    }
}
