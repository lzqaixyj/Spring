package com.lzq.hello;

import com.lzq.dao.UserDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 */
public class Application {
    public static void main(String[] args) {
//        MessageService messageService=new MessageService();
//        MessagePrinter messagePrinter=new MessagePrinter();
//        messagePrinter.setMessageService(messageService);
//        messagePrinter.print();
        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao = (UserDao) app.getBean("userDao");
        userDao.save();
    }
}
