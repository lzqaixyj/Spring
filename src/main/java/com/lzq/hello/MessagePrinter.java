package com.lzq.hello;

/**
 * 打印机
 */
public class MessagePrinter {
    private MessageService messageService;

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void print(){
        System.out.println(this.messageService.getMessage());
    }
}
