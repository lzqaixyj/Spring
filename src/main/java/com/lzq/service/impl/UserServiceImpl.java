package com.lzq.service.impl;

import com.lzq.dao.UserDao;
import com.lzq.dao.impl.UserDaoImpl;
import com.lzq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

//<bean id="userService" class="com.lzq.service.impl.UserServiceImpl">
//@Component("userService")
@Service("userService")
//@Scope("singleton")      配置bean的范围
@Scope("prototype")
public class UserServiceImpl implements UserService {
    /**
     * 通过注解注入dao
     */
    //<property name="userDao" ref="userDao"></property>
   // @Autowired  //按照数据类型从Spring容器中进行匹配
   // @Qualifier("userDao")  //按照id值从容器中匹配，但是必须要结合@Autowired 来使用
    @Resource(name = "userDao") //相当于 @Autowired + @Qualifier 合作使用
    private UserDao userDao;

    @Value("${jdbc.test.driver}")
    private String name;


    /**
     * set方法注入
     * @param userDao
     */
//    public void setUserDao(UserDao userDao) {
//        this.userDao = userDao;
//    }

    /**
     * 构造方法注入
     * @param userDao
     */
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    public void save() {
        System.out.println(name);
        userDao.save();
    }
}
