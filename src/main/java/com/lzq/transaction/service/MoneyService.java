package com.lzq.transaction.service;

import java.math.BigDecimal;

public interface MoneyService {
    void in(String inName, String outName, BigDecimal m);
}
