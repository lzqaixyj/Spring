package com.lzq.transaction.service.impl;

import com.lzq.transaction.dao.MoneyDao;
import com.lzq.transaction.service.MoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.REQUIRED)
public class MonytServiceImpl implements MoneyService {

    @Autowired
    private MoneyDao moneyDao;

    public void in(String inName, String outName, BigDecimal m) {
        moneyDao.in(inName,m);
        moneyDao.out(outName,m);
    }
}
