package com.lzq.transaction.dao.impl;

import com.lzq.transaction.dao.MoneyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

@Repository("moneyDao")
public class MoneyDaoImpl implements MoneyDao {
    @Autowired
    JdbcTemplate jdbcTemplate ;
    public void in(String name, BigDecimal m) {
        jdbcTemplate.update("update user set money=money+'"+m+"' where name = '"+name+"'");
    }

    public void out(String name, BigDecimal m) {
        jdbcTemplate.update("update user set money=money-'"+m+"' where name = '"+name+"'");
    }
}
