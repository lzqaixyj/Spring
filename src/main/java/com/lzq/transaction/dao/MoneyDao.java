package com.lzq.transaction.dao;

import java.math.BigDecimal;

public interface MoneyDao {
    void in(String name, BigDecimal m);
    void out(String name, BigDecimal m);
}
