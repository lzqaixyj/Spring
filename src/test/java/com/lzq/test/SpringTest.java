package com.lzq.test;

import com.lzq.dao.UserDao;
import com.lzq.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SpringTest {
    /**
     * 测试bean配置中，scope属性：单例 singleton ,多例 propotype
     * 单例只创建一个实例，多例创建了多个实例
     * singleton:实例化一次，在Spring核心文件加载时，实例化配置的Bean实例，
     *          生命周期：
     *          1、对象创建：当应用加载时，创建容器时，对象就被创建了
     *          2、对象运行：只要容器在，对象就一直活着
     *          3、对象销毁：当应用卸载，销毁容器时，对象就被销毁了
     * propotype:实例化多次，在调用getBean()时，实例化Bean
     *          生命周期：
     *          1、对象创建：使用对象时，创建新的对象实例
     *          2、对象运行：只要对象在使用中，就一直存在
     *          3、对象销毁：当对象长时间不用时，被java的垃圾回收器回收了
     */
    @Test
    public void springTest(){
        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao1 = (UserDao) app.getBean("userDao");
        UserDao userDao2 = (UserDao) app.getBean("userDao");
        System.out.println(userDao1==userDao2);
    }

    /**
     * bean 的三种实例化方法
     *      1、无参构造方法实例化
     *      2、工厂静态方法实例化
     *      3、工厂实例方法实例化
     */
    @Test
    public void test2(){
        ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao1 = (UserDao) app.getBean("userDao3");
        userDao1.save();
    }

    /**
     * 依赖注入：
     *       两种方式：1、通过set方法，将持久层dao配置到service
     *               2、通过有参构造方法
     */
    @Test
    public void test3(){
        ApplicationContext app =new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = (UserService)app.getBean("userService");
        userService.save();
    }

    /**
     * bean依赖注入的类型：
     *          1、引用数据类型，如dao配置到service；
     *          2、普通数据类型，如类中的属性设置
     *          3、集合数据类型，如List，Map，Properties
     */
    @Test
    public void test4(){
        ApplicationContext app =new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = (UserService)app.getBean("userService");
        userService.save();
    }

    /**
     * applicationContext实现类:
     *      1、ClassPathXmlApplicationContext ：类的根路径下（即resource下）
     *      2、FileSystemXmlApplicationContext ：文件在磁盘中的路径下
     *      3、AnnotationConfigApplicationContext：基于注解实现
     */
    @Test
    public void test5(){
        ApplicationContext app =new ClassPathXmlApplicationContext("applicationContext.xml");
        //ApplicationContext app = new FileSystemXmlApplicationContext("//Users/lizeqiang/workspace/workspace/springdemo/src/main/resources/applicationContext.xml");
        //ApplicationContext app = new AnnotationConfigApplicationContext("");
        UserService userService = (UserService)app.getBean("userService");
        userService.save();
    }

    /**
     * getBean()的两种实现：
     *      1、根据bean的id；
     *      2、根据class的方式；
     */
    @Test
    public void test6(){
        ApplicationContext app =new ClassPathXmlApplicationContext("applicationContext.xml");
        //UserService userService = (UserService)app.getBean("userService");
        UserService userService = app.getBean(UserService.class);
        userService.save();
    }
}
