package com.lzq.test;

import com.lzq.aop.Aop.Target;
import com.lzq.aop.Aop.TargetInterface;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-aop.xml")
public class AopTest {
    @Autowired
    TargetInterface target;
    @Test
    public void test1(){
        target.save();
    }
}
