package com.lzq.test;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.druid.pool.xa.DruidPooledXAConnection;
import com.lzq.config.SpringConfig;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * 数据源
 */
public class DataSourceTest {

    @Test
    //手动创建数据源  c3p0
    public void test(){
        ComboPooledDataSource pooledDataSource = new ComboPooledDataSource();
        try {
            pooledDataSource.setDriverClass("com.mysql.jdbc.Driver");
            pooledDataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
            pooledDataSource.setUser("root");
            pooledDataSource.setPassword("123456");

            Connection connection =pooledDataSource.getConnection();
            System.out.println(connection);
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    //手动创建数据源  druid (通过配置文件加载)
    public void test1() throws SQLException {
        //ResourceBundle只针对resource目录下的properties的文件进行读取，所以不需要拓展名
        ResourceBundle rb = ResourceBundle.getBundle("jdbc");
        String driver = rb.getString("jdbc.driver");
        String url = rb.getString("jdbc.url");
        String user = rb.getString("jdbc.user");
        String password = rb.getString("jdbc.password");
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(driver);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(user);
        druidDataSource.setPassword(password);

        DruidPooledConnection connection = druidDataSource.getConnection();
        System.out.println(connection);
        connection.close();
    }

    /**
     * Spring配置数据源
     */
    @Test
    public void test2() throws SQLException {
        ApplicationContext ap = new ClassPathXmlApplicationContext("jdbc.xml");
        DruidDataSource dataSource= (DruidDataSource) ap.getBean("dataSource");
        DruidPooledConnection connection = dataSource.getConnection();
        System.out.println(connection);
        connection.close();
    }

    /**
     * 注解方式实现数据源连接
     */
    @Test
    public void test3() throws SQLException {
        ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfig.class);
        DruidDataSource dataSource = (DruidDataSource) app.getBean("testDataSource");
        DruidDataSource dataSource1 = (DruidDataSource) app.getBean("demoDataSource");
        DruidPooledConnection dp = dataSource.getConnection();
        DruidPooledConnection dp1 = dataSource1.getConnection();
        System.out.println(dp);
        System.out.println(dp1);
        dp.close();
    }
}
