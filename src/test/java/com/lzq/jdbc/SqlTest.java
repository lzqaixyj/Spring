package com.lzq.jdbc;

import com.lzq.config.SpringConfig;
import com.lzq.transaction.service.MoneyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-transaction.xml")
//@ContextConfiguration(classes = SpringConfig.class)
public class SqlTest {
//    @Resource(name = "testDataSource")
//    DataSource dataSource;
    @Resource(name = "jdbcTemplate")
    JdbcTemplate jdbcTemplate;
    @Test
    public void test1(){
//        ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfig.class);
//        DataSource dataSource = (DataSource) app.getBean("testDataSource");
//        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        int row =jdbcTemplate.update("insert into user value (?,?)","lizeqiang",18);
        System.out.println(row);
    }
    @Test
    public void test2(){
//        ApplicationContext app = new AnnotationConfigApplicationContext(SpringConfig.class);
//        JdbcTemplate jdbcTemplate = (JdbcTemplate) app.getBean("jdbcConfig");
        int row =jdbcTemplate.update("insert into user value (?,?)","lizeqiang",18);
        System.out.println(row);
    }

    @Autowired
    private MoneyService moneyService;
    @Test
    public void test3(){
        moneyService.in("lizeqiang","lzq",new BigDecimal(1000));
    }
}
